
//
// File: probe_top.scad
// Vers: 1.0
//

linear_extrude( height = 5.5 ) {
  translate( [ 21.75, 26, 0 ] ) text( "Probe Red", size = 6, halign = "center" );
}  // end linear_extrude

difference() {
  cube( [ 43.5, 34.5, 5 ] );
  translate( [ 26, 5, 0 ] ) cylinder( h = 5, r = 1.25 );
  translate( [ 26, 20, 0 ] ) cylinder( h = 5, r = 1.25 );
}  // end difference