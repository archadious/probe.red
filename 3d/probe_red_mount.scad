
//
// File: probe_base.scad
// Vers: 1.5
//

//
// Base


// Payload
//translate( [ 2.25, 4.25, 2 ] ) cube( [ 35, 20, 7 ] );
//translate( [ 2.25, 24.5, 2 ] ) cube( [ 35, 20, 7 ] );

translate( [ 2, 4, 0 ] ) cube( [ 35, 40, 2 ] );

// Left and Right side
cube( [ 39, 4, 9.4 ] );
cube( [ 39, 2, 15 ] );
translate( [ 0, 44, 0 ] ) cube( [ 39, 4, 10 ] );
translate( [ 0, 46, 0 ] ) cube( [ 39, 2, 15 ] );

// Top and Bottom
cube( [ 2, 48, 15 ] );
translate( [ 37, 0, 0 ] ) cube( [ 2, 48, 15 ] );

//
// Top

//translate( [ 50, 4, 0 ] ) cube( [ 34.5, 43.5, 5 ] );

// Text
linear_extrude( height = 5.75 ) { 
  translate( [ 50, 24, 0 ] ) rotate( 90 ) {
    text( "Probe Red", size = 6, halign = "center", font = "FreeSerif:style=Bold" );
  } 
}  // end linear_extrude

difference() {
  translate( [ 42.25, 2.25, 0 ] ) cube( [ 34.5, 43.5, 5 ] );
  translate( [ 70, 28, 0 ] ) cylinder( h = 6, r = 1.25 );
  translate( [ 55, 28, 0 ] ) cylinder( h = 6, r = 1.25 );
}  // end difference


